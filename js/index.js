

//Se re quiere para el tooltip, difinir script de activacion dentro de la pagina, por default no se activa

 $(function(){
	$("[data-toggle='tooltip']").tooltip(); 
	$("[data-toggle='popover']").popover(); 
	$('.carousel').carousel({
		interval:2000 //para que rote mas rapido
	}); 
		
	$('#contacto').on('show.bs.modal',function(e){
		console.log('El modal se esta mostrando ');
		$('#contactoBtn').removeClass('btn-outline-success');
		$('#contactoBtn').addClass('btn-primary'); /*cuando ya se selecciono el boton cambia de color*/
		$('#contactoBtn').prop('disabled',true);/*cuando ya se selecciono el boton se deshabilita*/
	});
	$('#contacto').on('shown.bs.modal',function(e){
		console.log('El modal se esta mostro ');
	});
	$('#contacto').on('hide.bs.modal',function(e){
		console.log('El modal se esta ocultando ');
	});
	$('#contacto').on('hidden.bs.modal',function(e){
		console.log('El modal se Oculto! ');
		$('#contactoBtn').prop('disabled',false); /*cuando se cierra el form se habilita el boton*/
		$('#contactoBtn').removeClass('btn-primary');
		$('#contactoBtn').addClass('btn-outline-success');
	});
	
 });
 
 