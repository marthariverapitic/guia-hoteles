'use strict'

var gulp= require('gulp'),
    sass = require('gulp-sass'), 
    browserSync = require('browser-sync'),
    del = require('del'),
    imagemin = require('gulp-imagemin'),
    uglify = require('gulp-uglify'),
    usemin= require('gulp-usemin'),
    rev = require('gulp-rev'),
    cleancss = require('gulp-clean-css'),
    flatmap = require('gulp-flatmap'),
    htmlmin = require('gulp-htmlmin'); 


gulp.task('sass', function(){//definimod strict de gulp , objeto que al def una tarea usa el concepto de string
    gulp.src('./css/*.scss') //procesa flujo de archivos
        .pipe(sass().on('error', sass.logError))  //comienza armar la tarea sass
        .pipe(gulp.dest('./css')); ///termina con un dest donde lo va a guardar
});

gulp.task('sass:watch',function(){ //habilita el watch automatico
    gulp.watch('./css/*.scss', gulp.series('sass'));  //que corra la tarea y genere css
});


gulp.task('browser-sync',function(){// busca los html,csss img cualquier cambio y recarga la tarea
    var files = ['./*.html', './css/*.css', './imag/*.{png,jpg,gif,jpeg}', './js/*.js']; 
    browserSync.init(files, { 
        server:{
            baseDir:'./'
        }
    });
});


//corre la tarea del browser ye ejecture
gulp.task('default', gulp.parallel('browser-sync', 'sass:watch')); 

gulp.task('clean',function(){
    return del(['dist']); 
});

gulp.task('copyfonts',function(){
    return gulp.src('./node_modules/open-iconic/font/fonts/*.{ttf,woff,eof,svg,eot,otf}*') 
                .pipe(gulp.dest('./dist/fonts')); 
});

gulp.task('imagemin',function(){
    return gulp.src('./images/*.{png,jpg,jpeg,gif}')
        .pipe(imagemin({optimizationLevel: 3, progressive: true, interlaced: true}))
        .pipe(gulp.dest('dist/images')); 
});

gulp.task('usemin', function(){ 
    return gulp.src('*.html') 
                .pipe(flatmap(function(stream, file){ 
                    return stream
                        .pipe(usemin({
                            css:[rev()],
                            html: [function() {return htmlmin({collapseWhitespace: true})}], 
                            js: [uglify(), rev()], 
                            inlinejs: [uglify()], 
                            inlinecss: [cleancss(), 'concat'] 
                        }));
                }))
                .pipe(gulp.dest('dist/')); 
});


gulp.task('default', gulp.parallel('browser-sync', 'sass:watch')); 

gulp.task('build', gulp.series('clean','copyfonts','imagemin','usemin')); 
